﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Bovaa.Particles.MotionTracker
{
    class Animate
    {
        static void Main(string[] args)
        {
            char cont = 'y';
            do
            {
                try
                {
                    Console.Write("Enter speed:");
                    int speed = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter init for chamber:");
                    string init = Console.ReadLine();

                    var movementArray = AnimateChamber(speed, init);

                    movementArray.ToList().ForEach(i => Console.WriteLine(i.ToString()));

                    Console.Write("Continue (y/n)?");
                    cont = Console.ReadKey().KeyChar;
                    Console.Clear();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error", ex.Message);
                    Console.Write("Try a different input and continue (y/n)?");
                    cont = Console.ReadKey().KeyChar;
                    Console.Clear();
                }
            } while (cont == 'y' || cont == 'Y');
        }

        public static string[] AnimateChamber(int speed, string init)
        {
            string newChamberMap = string.Empty;
            Chamber chamber= new Chamber(init);

            List<string> movementArray = new List<string>() { chamber.GetChamberMap() };

            while (!chamber.IsEmpty())
            {
                chamber.Move(speed);
                newChamberMap = chamber.GetChamberMap();

                movementArray.Add(newChamberMap);
            }

            return movementArray.ToArray();
        }
    }
}
