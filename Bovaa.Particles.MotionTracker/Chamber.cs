﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Bovaa.Particles.MotionTracker
{
    interface IChamber
    {
        /// <summary>
        /// Moves the particles in the chamber n number of steps, where n is specified by the speed parameter.
        /// </summary>
        /// <param name="speed">number of steps that needs to be taken</param>
        /// <returns></returns>
        void Move(int speed);

        /// <summary>
        /// Returns true if the chamber doesn't have any particles
        /// </summary>
        /// <param name="init">the state of the chamber</param>
        /// <returns></returns>
        bool IsEmpty();

        /// <summary>
        /// Returns a visual representation of the chamber. 
        /// Occupied and empty spots will be marked appropriately.
        /// </summary>
        /// <returns></returns>
        string GetChamberMap();
    }

    public class Chamber : IChamber
    {
        #region static position markers
        public static char LEFTWARD_PARTICLE = 'L';
        public static char RIGHTWARD_PARTICLE = 'R';

        public static char EMPTY_SPOT = '.';
        public static char OCCUPIED_SPOT = 'X';
        #endregion

        // index of occupied spots is the key. Value is the list of particles that are at that index
        private SortedDictionary<int, List<char>> LocationHashmap { get; set; }
        private int stringLength { get; set; }

        public Chamber(string init)
        {
            stringLength = init.Length;

            LocationHashmap = new SortedDictionary<int, List<char>>();
            for (int i=0; i<init.Length; i++)
            {
                if (init[i] == LEFTWARD_PARTICLE || init[i] == RIGHTWARD_PARTICLE)
                {
                    if (!LocationHashmap.ContainsKey(i))
                    {
                        LocationHashmap.Add(i, new List<char>());
                    }
                    LocationHashmap[i].Add(init[i]);
                }
            }
        }

        public bool IsEmpty()
        {
            return LocationHashmap == null || LocationHashmap.Count == 0;
        }

        public void Move(int speed)
        {
            if (speed > 0)
            {
                SortedDictionary<int, List<char>> newLocationMap = new SortedDictionary<int, List<char>>();

                foreach (var index in LocationHashmap.Keys)
                {
                    foreach (var particle in LocationHashmap[index])
                    {
                        if (particle.Equals(RIGHTWARD_PARTICLE) && index + speed < stringLength)
                        {
                            if (!newLocationMap.Keys.Contains(index + speed))
                                newLocationMap.Add(index + speed, new List<char>());

                            newLocationMap[index + speed].Add(RIGHTWARD_PARTICLE);
                        }
                        else if (particle.Equals(LEFTWARD_PARTICLE) && index - speed >= 0)
                        {
                            if (!newLocationMap.Keys.Contains(index - speed))
                                newLocationMap.Add(index - speed, new List<char>());

                            newLocationMap[index - speed].Add(LEFTWARD_PARTICLE);
                        }
                    }
                }

                LocationHashmap = newLocationMap;
            }
        }

        public string GetChamberMap()
        {
            StringBuilder chamberMap = new StringBuilder();
            for(int i=0; i< stringLength; i++)
            {
                if (LocationHashmap.ContainsKey(i))
                    chamberMap.Append(OCCUPIED_SPOT);
                else
                    chamberMap.Append(EMPTY_SPOT);
            }

            return chamberMap.ToString();
        }
    }
}
