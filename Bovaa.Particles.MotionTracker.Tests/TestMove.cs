﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bovaa.Particles.MotionTracker;

namespace Bovaa.Particles.MotionTracker.Tests
{
    [TestClass]
    public class TestMove
    {
        [TestMethod]
        public void TestMoveWithOneRightParticle()
        {
            string init = "..R....";
            string expectedMap = "....X..";
            int speed = 2;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }
        [TestMethod]
        public void TestMoveWithOneLeftParticle()
        {
            string init = ".....L.";
            string expectedMap = "...X...";
            int speed = 2;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }

        [TestMethod]
        public void TestMoveWithMultipleParticles()
        {
            string init = "RR..LRL";
            string expectedMap = ".X.XX..";
            int speed = 3;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }

        [TestMethod]
        public void TestMoveWithOneRightParticleAndOutOfBoundsSpeed()
        {
            string init = "..R....";
            string expectedMap = ".......";
            int speed = 50;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }

        [TestMethod]
        public void TestMoveWithOneLeftParticleAndOutOfBoundsSpeed()
        {
            string init = "..L....";
            string expectedMap = ".......";
            int speed = 50;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }


        [TestMethod]
        public void TestMoveWithFullyOccupiedChamber_AndEmptyInOneStepSpeed()
        {
            string init = "XXXXXXXXXX";
            string expectedMap = "..........";
            int speed = 10;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }


        [TestMethod]
        public void TestMoveWithEmptyChamber()
        {
            string init = "...";
            string expectedMap = "...";
            int speed = 10;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }
        [TestMethod]
        public void TestMoveWithNegativeSpeed_ShouldDoNothing()
        {
            string init = "..L....";
            string expectedMap = "..X....";
            int speed = -1;
            Chamber chamber = new Chamber(init);
            chamber.Move(speed);
            string actualMap = chamber.GetChamberMap();

            Assert.AreEqual(expectedMap, actualMap);
        }
    }
}
